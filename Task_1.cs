﻿using System;

namespace Lab_2
{
    class Task_1
    {
        static void Main(string[] args)
        {
            //Дана сторона ромба и противоположные углы ,найти площадь,периметр, длину диагоналей
            Console.WriteLine("Введите сторону ромба");
            double rhonbSide = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите угол ромаба");
            double rhonbAngle = Convert.ToDouble(Console.ReadLine());

            //Конвертация угла в радианы 
            double triangleAngle = rhonbAngle / 2;
            double triangleAngleRadians = triangleAngle * Math.PI / 180;

            //Периметр
            double perimeter = 4 * rhonbSide;
            Console.WriteLine("Периметр: " + perimeter);

            //Катеты треугольника
            double firstCathetTriangle = rhonbSide * Math.Sin(triangleAngleRadians);
            double secondCathetTriangle = rhonbSide * Math.Sin(triangleAngleRadians);

            //Длины диагоналей
            double firstDiagonal = firstCathetTriangle * 2;
            Console.WriteLine("Длина первой диагонали: " + firstDiagonal);
            double secondDiagonal = secondCathetTriangle * 2;
            Console.WriteLine("Длина второй диагонали: " + secondDiagonal);

            //Площадь
            double square = (firstDiagonal * secondDiagonal) / 2;
            Console.WriteLine("Площадь: " + square);
            Console.ReadKey();
        }
    
    }
}
