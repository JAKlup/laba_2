﻿using System;

namespace Lab_2_Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Выводим информацию на консоль.
            // Ждем от пользователя ввода строки для x.
            Console.WriteLine("Введите x:");
            string xstr = Console.ReadLine();

            // Ждем от пользователя ввода строки для y.
            Console.WriteLine("Введите y:");
            string ystr = Console.ReadLine();

            // Ждем от пользователя ввода строки для z.
            Console.WriteLine("Введите z:");
            string zstr = Console.ReadLine();

            // Конвертируем строковой тип в тип double.
            double x = Convert.ToDouble(xstr);

            // Конвертируем строковой тип в тип double.
            double y = Convert.ToDouble(ystr);

            // Конвертируем строковой тип в тип double.
            double z = Convert.ToDouble(zstr);

            // Объявляем переменную f.
            double f = 1 / 13.0 + Math.Log(Math.Abs(y / 3)) + z * 7 + Math.Max(3 * x, 9 * x);

            Console.WriteLine(f);

            Console.ReadKey();
        }
    }
}
