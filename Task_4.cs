using System;

namespace Lab_2_Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            // Поезд
            Console.WriteLine("цифравая модель поезда");

            // Заданные атрибуты цифровой модели
            // Длина вагона поезда (м).            
            double vanLength;
            // Ширина вагона поезда (м). 
            double vanWidth;
            // Высота вагона поезда (м). 
            double vanHeight;
            // Длина локомотива (м).
            double trainLength;
            // Ширина локомотива (м).
            double trainWidth;
            // Высота локомотива.
            double trainHeight;
            // Количество ходовых частей вагона поезда. 
            int vanRunningGearsVanCount;
            // Колличесвто ходовых частей локомотива.
            int trainRunningGearsCount;
            // Количество купе в вагоне поезда. 
            int compartmentCount;
            // Количество пассажирских мест в купе поезда. 
            int passengerPlaceCount;
            // Количесвто вагонов поезда 
            int vanCount;
            // Тип двигателя локомотива.
            int enginetype;

            // Вычисляемые атрибуты цифровой модели
            // Объем вагона
            double vanVolume;
            // Объём локомотива
            double traineVolume;
            // Общий объём поезда
            double trainVolumeCommon;
            // Общее количесвто ходовых частей поезда
            double runningGearsCommon;
            // Общее колличесвто пассажиров
            double passengers;
            // Общая длина состава 
            double lenghtCommon;
            // Ввод информации в программу.         
            Console.WriteLine("Введите длину вагона:");
            string lenghtVanStr = Console.ReadLine();
            vanLength = Convert.ToDouble(lenghtVanStr);

            Console.WriteLine("Введите ширину вагона:");
            string widthVanStr = Console.ReadLine();
            vanWidth = Convert.ToDouble(widthVanStr);

            Console.WriteLine("Введите высоту вагона:");
            string heightVanStr = Console.ReadLine();
            vanHeight = Convert.ToDouble(heightVanStr);

            Console.WriteLine("Введите длину локомотива:");
            string lengthTrainStr = Console.ReadLine();
            trainLength = Convert.ToDouble(lengthTrainStr);

            Console.WriteLine("Введите ширину локомотива:");
            string widthTrainStr = Console.ReadLine();
            trainWidth = Convert.ToDouble(widthTrainStr);

            Console.WriteLine("Введите высоту локомотива:");
            string heightTrainStr = Console.ReadLine();
            trainHeight = Convert.ToDouble(heightTrainStr);

            Console.WriteLine("Введите количесвто вагонов:");
            string vanCountStr = Console.ReadLine();
            vanCount = (int)Convert.ToDouble(vanCountStr);

            Console.WriteLine("Введите число ходовых частей вагона:");
            string runningGearsVanCountStr = Console.ReadLine();
            vanRunningGearsVanCount = Convert.ToInt32(runningGearsVanCountStr);

            Console.WriteLine("Введите число ходовых частей локомотива:");
            string runningGearsTrainCountStr = Console.ReadLine();
            trainRunningGearsCount = Convert.ToInt32(runningGearsTrainCountStr);

            Console.WriteLine("Введите колличество купе в поезде:");
            string compartmentCountStr = Console.ReadLine();
            compartmentCount = Convert.ToInt32(compartmentCountStr);

            Console.WriteLine("Введите колличесвто пассажирских мест в поезде:");
            string passengerPlaceCountStr = Console.ReadLine();
            passengerPlaceCount = Convert.ToInt32(passengerPlaceCountStr);

            Console.Write("Введите тип двигателя: ");
            string name = Console.ReadLine();

            // Расчет вычисляемых атрибутов.
            vanVolume = vanLength * vanWidth * vanHeight;
            traineVolume = trainLength * trainWidth * trainHeight;
            trainVolumeCommon = (vanVolume * vanCount) + traineVolume;
            runningGearsCommon = (vanRunningGearsVanCount * vanCount) + trainRunningGearsCount;
            passengers = compartmentCount * passengerPlaceCount * vanCount;
            lenghtCommon = (vanLength * vanCount) + trainLength;

            // Вывод информации на консоль.
            Console.WriteLine("длина вагона : " + vanLength + " м.");
            Console.WriteLine("ширина вагона : " + vanWidth + "м");
            Console.WriteLine("высота вагона : " + vanHeight + " м.");
            Console.WriteLine("длина локомотива : " + trainLength + " м.");
            Console.WriteLine("ширина локомотива : " + trainWidth + "м");
            Console.WriteLine("высота локомотива : " + trainHeight + " м.");
            Console.WriteLine("общая длина состава: " + lenghtCommon + "м.");
            Console.WriteLine("число ходовых частей вагона поезда: " + vanRunningGearsVanCount);
            Console.WriteLine("число ходовых частей поезда: " + trainRunningGearsCount);
            Console.WriteLine("общее количесвто ходовых частей состава: " + runningGearsCommon);
            Console.WriteLine("число купе в вагоне поезда: " + compartmentCount);
            Console.WriteLine("колличесвто пассажирских мест в поезда: " + passengerPlaceCount);
            Console.WriteLine("общее колличесвто пассажиров: " + passengers);
            Console.WriteLine("объём вагона : " + vanVolume + " м^3.");
            Console.WriteLine("объём локомотива  : " + traineVolume + " м^3.");
            Console.WriteLine("общий объём состава : " + trainVolumeCommon + " м^3.");
            Console.WriteLine("тип двигателя локомотива: " + name);
        }
    }
}
