﻿using System;

namespace Lab_2_Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Выводим информацию на консоль.
            Console.WriteLine("Введите x:");

            // Ждем от пользователя ввода строки.
            string xstr = Console.ReadLine();

            // Конвертируем строковой тип в тип double.
            double x = Convert.ToDouble(xstr);

            // Объявляем y
            double f = 1 / 13.0 + Math.Log(Math.Abs(x / 3)) + 7 * x + Math.Max(3 * x, 9 * x) + 2 * Math.Tan(x / 3) / +Math.Pow(2, x) + 1;

            Console.WriteLine(f);
        }
    }
}
